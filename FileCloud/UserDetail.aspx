﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage1.Master" AutoEventWireup="true" CodeBehind="UserDetail.aspx.cs" Inherits="FileCloud.UserDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<br />
	<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">
	  로그인 기록</div>
&nbsp;유저 기본 정보<br />
	<asp:DetailsView ID="DetailsView1" runat="server" Height="50px" Width="100%" AutoGenerateRows="False" DataKeyNames="uid" DataSourceID="SqlDataSource1" CssClass="table table-hover table-striped " GridLines="None">
		<Fields>
			<asp:BoundField DataField="uid" HeaderText="uid" InsertVisible="False" ReadOnly="True" SortExpression="uid" />
			<asp:BoundField DataField="username" HeaderText="username" SortExpression="username" />
			<asp:BoundField DataField="age" HeaderText="age" SortExpression="age" />
			<asp:BoundField DataField="gender" HeaderText="gender" SortExpression="gender" />
			<asp:BoundField DataField="level" HeaderText="level" SortExpression="level" />
			<asp:BoundField DataField="point" HeaderText="point" SortExpression="point" />
			<asp:BoundField DataField="reg_date" HeaderText="reg_date" SortExpression="reg_date" />
		</Fields>
	</asp:DetailsView>
		</div>
	<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CloudConnectionString %>" SelectCommand="SELECT [uid], [username], [age], [gender], [level], [point], [reg_date] FROM [Users] WHERE ([uid] = @uid)">
		<SelectParameters>
			<asp:SessionParameter DefaultValue="1" Name="uid" SessionField="user_id" Type="Int32" />
		</SelectParameters>
	</asp:SqlDataSource>
	<br />
	<br />
	<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">
	  로그인 기록</div>
	<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource2" CssClass="table table-hover table-striped " GridLines="None">
		<Columns>
			<asp:BoundField DataField="timestamp" HeaderText="timestamp" SortExpression="timestamp" />
			<asp:BoundField DataField="address" HeaderText="address" SortExpression="address" />
			<asp:BoundField DataField="success" HeaderText="success" SortExpression="success" />
		</Columns>
	</asp:GridView>
	<asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:CloudConnectionString %>" SelectCommand="SELECT [timestamp], [address], [success] FROM [login_log] WHERE ([uid] = @uid)">
		<SelectParameters>
			<asp:SessionParameter DefaultValue="1" Name="uid" SessionField="user_id" Type="Int32" />
		</SelectParameters>
	</asp:SqlDataSource>
		</div>
</asp:Content>
