﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FileCloud
{
	public partial class index : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e) {
			Session["login_success"] = Session["login_success"] == null ? false : Session["login_success"];
			Session["user_id"] = Session["user_id"] == null ? "" : Session["user_id"];
			Session["user_level"] = Session["user_level"] == null ? "" : Session["user_level"];
			if((bool)Session["login_success"]) {
				Response.Redirect("/Cloud.aspx");
			}
		}
	}
}