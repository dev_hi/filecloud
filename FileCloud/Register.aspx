﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage1.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="FileCloud.Register" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="/css/reg.css" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div id="reg_form">
		<h1>회원가입</h1>
		<div class="input-group">
			<asp:TextBox ID="username" runat="server" class="form-control" placeholder="Username" />
			<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="username" ErrorMessage="&lt;span style=&quot;color:red&quot;&gt;이름을 입력하세요.&lt;/span&gt;"></asp:RequiredFieldValidator>
			<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="username" ErrorMessage="&lt;span style=&quot;color:red&quot;&gt;이름은 영어나 숫자 4자 이상입니다.&lt;/span&gt;" ValidationExpression="[a-zA-Z0-9]{4,}"></asp:RegularExpressionValidator>
		</div>
		<div class="input-group">
			<asp:TextBox ID="Password" TextMode="Password" runat="server" class="form-control" placeholder="Password" />
			<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Password" ErrorMessage="&lt;span style=&quot;color:red&quot;&gt;비밀번호를 입력하세요.&lt;/span&gt;"></asp:RequiredFieldValidator>
		</div>
		<div class="input-group">
			<asp:TextBox ID="Password_confirm" TextMode="Password" runat="server" class="form-control" placeholder="Confirm Password" />
			<asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="Password" ControlToValidate="Password_confirm" ErrorMessage="&lt;span style=&quot;color:red&quot;&gt;비밀번호를 똑같이 입력하세요.&lt;/span&gt;"></asp:CompareValidator>
		</div>
		<div class="input-group">
			<asp:TextBox ID="age" TextMode="Number" runat="server" class="form-control" placeholder="나이" />
			<asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="age" ErrorMessage="&lt;span style=&quot;color:red&quot;&gt;1~400까지 입력해주세요.&lt;/span&gt;" MaximumValue="400" MinimumValue="1" Type="Integer"></asp:RangeValidator>
			<span class="input-group-addon" id="basic-addon2">세</span>
		</div>
		<div class="input-group">
			<asp:DropDownList ID="gender" runat="server" CssClass="form-control">
				<asp:ListItem Value="0">남자</asp:ListItem>
				<asp:ListItem Value="1">여자</asp:ListItem>
			</asp:DropDownList>
		</div>
		<div>
			<asp:Button ID="Button1" runat="server" Text="제출" CssClass="btn btn-primary" OnClick="Button1_Click" />
			<asp:Button ID="Button2" runat="server" Text="취소" CssClass="btn btn-danger" OnClick="Button2_Click" />
		</div>
	</div>
	
</asp:Content>
