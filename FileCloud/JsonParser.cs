﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Globalization;

namespace FileCloud
{
	public class JsonParser {
		//const string REGEX_PATTERN = "{|}|\\[|\\]|true|false|null|,|-*[\\d.]+|\"[^,\\n{}\\[\\]:]*\"+:*";
		const string REGEX_PATTERN = "{|}|\\[|\\]|true|false|null|,|-*[\\d.]+|\"(\\\\\"|[^\"])*\":*";
		const string ESCAPED_UNICODE_PATTERN = @"\\[uU]([0-9A-Fa-f]{4})";
		public JsonObject RootObject;
		private string OriginalString;

		public Exception LatestException;

		public JsonParser(string json) {
			this.OriginalString = json;
		}

		public bool TryParse(out JsonObject output) {
			try {
				output = this.Parse();
				return true;
			}
			catch(Exception e) {
				this.LatestException = e;
				output = null;
				return false;
			}
		}

		public JsonObject Parse() {
			Regex regexr = new Regex(REGEX_PATTERN);
			MatchCollection splited = regexr.Matches(this.OriginalString);

			if(splited.Count == 0) {
				throw new Exception("JSON형식의 요소를 발견할 수 없었습니다.");
			}

			int current_depth = 0;
			JsonObject current_object = new JsonObject();
			this.RootObject = current_object;
			//this.RootObject.Parent = this.RootObject;

			foreach(Match piece in splited) {
				string c = piece.Value;
				int i = piece.Index;
				double f = 0;
				if(c == "{") {
					//node adder & object start
					current_depth++;
					if(current_object.Type == JsonObjectType.array) {
						JsonObject new_object = new JsonObject(new Dictionary<string, JsonObject>(), JsonObjectType.@object);
						JsonObject parent = current_object;
						new_object.Parent = parent;
						((List<JsonObject>)current_object.Value).Add(new_object);
						current_object = new_object;
					}
					else {
						current_object.Type = JsonObjectType.@object;
						current_object.Value = (object)(new Dictionary<string, JsonObject>());
					}
				}
				else if(c == "}") {
					//node subtractor & end of object
					current_depth--;
					current_object = current_object.Parent;
				}
				else if(c == "[") {
					//node adder & array start
					current_depth++;
					if(current_object.Type == JsonObjectType.array) {
						JsonObject new_object = new JsonObject(new List<JsonObject>(), JsonObjectType.array);
						JsonObject parent = current_object;
						new_object.Parent = parent;
						((List<JsonObject>)current_object.Value).Add(new_object);
						current_object = new_object;
					}
					else {
						current_object.Type = JsonObjectType.array;
						current_object.Value = (object)(new List<JsonObject>());
					}
				}
				else if(c == "]") {
					//node substrator & end of array
					current_depth--;
					current_object = current_object.Parent;
				}
				else if(c == ",") {
					//divider
					
				}
				else if(c[c.Length - 1] == ':') {
					//object initialize
					string name = this.EscapedUnicodeToString(c.Substring(1, c.LastIndexOf('\"') - 1));
					JsonObject new_object = new JsonObject(name);
					JsonObject parent = current_object;
					new_object.Parent = parent;
					if(current_object.Type == JsonObjectType.@object) {
						((Dictionary<string, JsonObject>)current_object.Value).Add(name, new_object);
					}
					else if(current_object.Type == JsonObjectType.array) {
						((List<JsonObject>)current_object.Value).Add(new_object);
					}
					current_object = new_object;
				}


				/*else if(int.TryParse(c, out i)) {
					//number(int)
					current_object.Type = JsonObjectType.number;
					current_object.Value = (object)i;
					current_object = current_object.Parent;
				}*/


				else if(double.TryParse(c, out f)) {
					//number(double)
					if(current_object.Type == JsonObjectType.array) {
						((List<JsonObject>)current_object.Value).Add(new JsonObject((object)f, JsonObjectType.number));
					}
					else {
						current_object.Type = JsonObjectType.number;
						current_object.Value = (object)f;
						current_object = current_object.Parent;
					}
				}
				else if(c == "true") {
					//boolean of true
					if(current_object.Type == JsonObjectType.array) {
						((List<JsonObject>)current_object.Value).Add(new JsonObject((object)true, JsonObjectType.boolean));
					}
					else {
						current_object.Type = JsonObjectType.boolean;
						current_object.Value = (object)true;
						current_object = current_object.Parent;
					}
				}
				else if(c == "false") {
					//boolean of false
					if(current_object.Type == JsonObjectType.array) {
						((List<JsonObject>)current_object.Value).Add(new JsonObject((object)false, JsonObjectType.boolean));
					}
					else {
						current_object.Type = JsonObjectType.boolean;
						current_object.Value = (object)false;
						current_object = current_object.Parent;
					}
				}
				else if(c == "null") {
					//null object
					if(current_object.Type == JsonObjectType.array) {
						((List<JsonObject>)current_object.Value).Add(new JsonObject((object)null, JsonObjectType.@null));
					}
					else {
						current_object.Type = JsonObjectType.@null;
						current_object.Value = (object)null;
						current_object = current_object.Parent;
					}
				}
				else if(c[0] == '\"' && c[c.Length - 1] == '\"') {
					//general string
					string strdata = this.EscapedUnicodeToString(c.Trim(new char[] { '"' }));
					if(current_object.Type == JsonObjectType.array) {
						((List<JsonObject>)current_object.Value).Add(new JsonObject((object)strdata, JsonObjectType.@string));
					}
					else {
						current_object.Type = JsonObjectType.@string;
						current_object.Value = (object)strdata;
						current_object = current_object.Parent;
					}
				}
			}

			return this.RootObject;
		}

		public string EscapedUnicodeToString(string orig) {
			string ret = orig;
			Regex unicode_extract = new Regex(ESCAPED_UNICODE_PATTERN);
			Match match = unicode_extract.Match(ret);
			while(match.Success) {
				string rep = ret.Substring(0, match.Index) + ((char)Int32.Parse(match.Value.Substring(2), NumberStyles.HexNumber)).ToString() + ret.Substring(match.Index + match.Length);
				ret = rep;
				match = unicode_extract.Match(ret);
			}

			return ret;
		}
	}
}
