﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Collections;
using System.Xml;
using System.Web.UI;

namespace FileCloud {
	public class Global : System.Web.HttpApplication {
		public static ArrayList Cur_user;

		protected void Application_Start(object sender, EventArgs e) {
			ScriptManager.ScriptResourceMapping.AddDefinition("jquery", new ScriptResourceDefinition {
				Path = "~/js/jquery-2.1.3.min.js"
			});
			//전체 방문자 카운트
			//Application["count"] = 0;
			Application["count"] = counter;
			//현재 방문자수
			Application["cur_count"] = 0;

			//방문자 리스트
			if(Cur_user == null) Cur_user = new ArrayList();
			Application.Add("userlist", Cur_user);
		}

		protected void Session_Start(object sender, EventArgs e) {
			//전체 방문자 카운트
			Application.Lock();
			counter++;
			Application["count"] = counter;
			//Application["count"] = 1 + (int)Application["count"];

			//현재 방문자수
			Application["cur_count"] = 1 + (int)Application["cur_count"];
			Application.UnLock();
		}

		protected void Application_BeginRequest(object sender, EventArgs e) {

		}

		protected void Application_AuthenticateRequest(object sender, EventArgs e) {

		}

		protected void Application_Error(object sender, EventArgs e) {

		}

		protected void Session_End(object sender, EventArgs e) {
			Application.Lock();
			Application["cur_count"] = (int)Application["cur_count"] - 1;

			//로그아웃하면 방문리스트에서 제거
			string username = Session["username"].ToString();
			Cur_user.Remove(username);
			Application.UnLock();
		}

		protected void Application_End(object sender, EventArgs e) {

		}

		protected int counter {
			get {
				int ret = 0;
				XmlDocument xml = new XmlDocument();
				xml.Load(Server.MapPath("/Settings.xml"));
				XmlNodeList node_list = xml["settings"]["status"].ChildNodes;
				foreach(XmlNode node in node_list) {
					if(node.Name == "key" && node.Attributes["name"].Value == "total_visited") {
						ret = int.Parse(node.InnerText);
					}
				}
				return ret;
			}
			set {
				XmlDocument xml = new XmlDocument();
				string path = Server.MapPath("/Settings.xml");
                xml.Load(path);
				XmlNodeList node_list = xml["settings"]["status"].ChildNodes;
				foreach(XmlNode node in node_list) {
					if(node.Name == "key" && node.Attributes["name"].Value == "total_visited") {
						node.InnerText = value.ToString();
					}
				}
				xml.Save(path);
			}
		}
	}
}