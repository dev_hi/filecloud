﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage1.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="FileCloud.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="/css/Login.css" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

	<div id="login_form">
		<img src="img/identification57.png" width="40%" />
		<h1>로그인</h1><br />
		<div class="input-group">
			<input runat="server" type="text" id="username" class="form-control" placeholder="유저이름" />
			<input runat="server" type="password" id="password" class="form-control" placeholder="비밀번호" />
		</div>
		<asp:Button ID="Button1" runat="server" Text="로그인" CssClass="btn btn-default" OnClick="Button1_Click"/>
		<asp:Button ID="Button2" runat="server" Text="회원가입" CssClass="btn btn-default" OnClick="Button2_Click" PostBackUrl="~/Register.aspx"/>
	</div>

</asp:Content>
