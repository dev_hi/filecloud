﻿/* Cloud */
CREATE DATABASE [Cloud]
GO

/* Cloud */
USE [Cloud]
GO

/* Cloud */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'Cloud'
GO

/* 주파일그룹 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'주파일그룹', 
	@level0type=N'FILEGROUP', @level0name=N'PRIMARY'
GO

/* 유저 */
CREATE TABLE [Users] (
	[uid] [int] NOT NULL IDENTITY (1,  1),  /* 유저 고유 ID */
	[username] [text],  /* 유저 이름 */
	[password] [varbinary](max),  /* 유저 비밀번호 */
	[posted] [int] NOT NULL DEFAULT 0,  /* 올린 글 수 */
	[age] [int],  /* 나이 */
	[gender] [tinyint],  /* 성별 */
	[level] [char](1) NOT NULL DEFAULT 'C',  /* 유저 등급 */
	[point] [int] NOT NULL DEFAULT 0,  /* 포인트 */
	[token_id] [int],  /* 토큰 식별 ID */
	[reg_date] [date] DEFAULT GETDATE() /* 가입 날짜 */
)
GO

/* 유저 기본키 */
ALTER TABLE [Users]
	ADD
		CONSTRAINT [PK_Users]
		PRIMARY KEY NONCLUSTERED (
			[uid] ASC
		)
GO

/* 유저 유니크 제약 */
ALTER TABLE [Users]
	ADD
		CONSTRAINT [UK_Users]
		UNIQUE NONCLUSTERED (
			[username] ASC
		)
GO

/* 유저 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'유저', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Users'
GO

/* 유저 고유 ID */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'유저 고유 ID', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Users', 
	@level2type=N'COLUMN', @level2name=N'uid'
GO

/* 유저 이름 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'유저 이름', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Users', 
	@level2type=N'COLUMN', @level2name=N'username'
GO

/* 유저 비밀번호 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'유저 비밀번호', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Users', 
	@level2type=N'COLUMN', @level2name=N'password'
GO

/* 올린 글 수 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'올린 글 수', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Users', 
	@level2type=N'COLUMN', @level2name=N'posted'
GO

/* 나이 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'나이', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Users', 
	@level2type=N'COLUMN', @level2name=N'age'
GO

/* 성별 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'성별', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Users', 
	@level2type=N'COLUMN', @level2name=N'gender'
GO

/* 유저 등급 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'유저 등급', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Users', 
	@level2type=N'COLUMN', @level2name=N'level'
GO

/* 포인트 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'포인트', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Users', 
	@level2type=N'COLUMN', @level2name=N'point'
GO

/* 토큰 식별 ID */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'토큰 식별 ID', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Users', 
	@level2type=N'COLUMN', @level2name=N'token_id'
GO

/* 가입 날짜 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'가입 날짜', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Users', 
	@level2type=N'COLUMN', @level2name=N'reg_date'
GO

/* 유저 기본키 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'유저 기본키', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Users', 
	@level2type=N'CONSTRAINT', @level2name=N'PK_Users'
GO

/* 유저 유니크 제약 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'유저 유니크 제약', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Users', 
	@level2type=N'CONSTRAINT', @level2name=N'UK_Users'
GO

/* 유저 기본키 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'유저 기본키', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Users', 
	@level2type=N'INDEX', @level2name=N'PK_Users'
GO

/* 유저 유니크 인덱스 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'유저 유니크 인덱스', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Users', 
	@level2type=N'INDEX', @level2name=N'UIX_Users'
GO

/* 페이스북 토큰 */
CREATE TABLE [Facebook_Token] (
	[token_id] [int] NOT NULL IDENTITY (1,  1),  /* 토큰 식별 ID */
	[token_key] [text] NOT NULL,  /* 토큰 키 */
	[token_secret] [text] NOT NULL,  /* 토큰 시크릿 */
	[uid] [int] /* 유저 고유 ID */
)
GO

/* 페이스북 토큰 기본키 */
ALTER TABLE [Facebook_Token]
	ADD
		CONSTRAINT [PK_Facebook_Token]
		PRIMARY KEY NONCLUSTERED (
			[token_id] ASC
		)
GO

/* 페이스북 토큰 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'페이스북 토큰', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Facebook_Token'
GO

/* 토큰 식별 ID */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'토큰 식별 ID', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Facebook_Token', 
	@level2type=N'COLUMN', @level2name=N'token_id'
GO

/* 토큰 키 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'토큰 키', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Facebook_Token', 
	@level2type=N'COLUMN', @level2name=N'token_key'
GO

/* 토큰 시크릿 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'토큰 시크릿', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Facebook_Token', 
	@level2type=N'COLUMN', @level2name=N'token_secret'
GO

/* 유저 고유 ID */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'유저 고유 ID', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Facebook_Token', 
	@level2type=N'COLUMN', @level2name=N'uid'
GO

/* 페이스북 토큰 기본키 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'페이스북 토큰 기본키', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Facebook_Token', 
	@level2type=N'CONSTRAINT', @level2name=N'PK_Facebook_Token'
GO

/* 페이스북 토큰 기본키 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'페이스북 토큰 기본키', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Facebook_Token', 
	@level2type=N'INDEX', @level2name=N'PK_Facebook_Token'
GO

/* 파일 */
CREATE TABLE [Files] (
	[file_id] [int] NOT NULL IDENTITY (1,  1),  /* 파일 고유 ID */
	[uid] [int] NOT NULL,  /* 유저 고유 ID */
	[dir_id] [int],  /* 폴더 ID */
	[filename] [ntext] NOT NULL,  /* 파일 이름 */
	[filesize] [int] NOT NULL,  /* 파일 크기 */
	[hash] [text] NOT NULL,  /* 파일 해시 */
	[upload_time] [datetime] NOT NULL,  /* 업로드 시간 */
	[physical_file] [text] NOT NULL /* 실제 파일 경로 */
)
GO

/* 파일 기본키 */
ALTER TABLE [Files]
	ADD
		CONSTRAINT [PK_Files]
		PRIMARY KEY NONCLUSTERED (
			[file_id] ASC, 
			[uid] ASC
		)
GO

/* 파일 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'파일', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Files'
GO

/* 파일 고유 ID */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'파일 고유 ID', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Files', 
	@level2type=N'COLUMN', @level2name=N'file_id'
GO

/* 유저 고유 ID */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'유저 고유 ID', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Files', 
	@level2type=N'COLUMN', @level2name=N'uid'
GO

/* 폴더 ID */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'폴더 ID', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Files', 
	@level2type=N'COLUMN', @level2name=N'dir_id'
GO

/* 파일 이름 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'파일 이름', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Files', 
	@level2type=N'COLUMN', @level2name=N'filename'
GO

/* 파일 크기 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'byte단위', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Files', 
	@level2type=N'COLUMN', @level2name=N'filesize'
GO

/* 파일 해시 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'파일 해시', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Files', 
	@level2type=N'COLUMN', @level2name=N'hash'
GO

/* 업로드 시간 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'업로드 시간', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Files', 
	@level2type=N'COLUMN', @level2name=N'upload_time'
GO

/* 실제 파일 경로 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'실제 파일 경로', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Files', 
	@level2type=N'COLUMN', @level2name=N'physical_file'
GO

/* 파일 기본키 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'파일 기본키', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Files', 
	@level2type=N'CONSTRAINT', @level2name=N'PK_Files'
GO

/* 파일 기본키 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'파일 기본키', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Files', 
	@level2type=N'INDEX', @level2name=N'PK_Files'
GO

/* 게시글 */
CREATE TABLE [Documents] (
	[doc_id] [int] NOT NULL IDENTITY (1,  1),  /* 게시글 ID */
	[cat_id] [int] NOT NULL,  /* 카테고리 ID */
	[uid] [int] NOT NULL,  /* 유저 고유 ID */
	[doc_title] [ntext] NOT NULL,  /* 게시글 제목 */
	[content] [ntext],  /* 내용 */
	[post_time] [datetime],  /* 올린 시간 */
	[file_list] [text] /* 연결된 파일들 */
)
GO

/* 게시글 기본키 */
ALTER TABLE [Documents]
	ADD
		CONSTRAINT [PK_Documents]
		PRIMARY KEY NONCLUSTERED (
			[doc_id] ASC, 
			[cat_id] ASC, 
			[uid] ASC
		)
GO

/* 게시글 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'게시글', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Documents'
GO

/* 게시글 ID */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'게시글 ID', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Documents', 
	@level2type=N'COLUMN', @level2name=N'doc_id'
GO

/* 카테고리 ID */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'카테고리 ID', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Documents', 
	@level2type=N'COLUMN', @level2name=N'cat_id'
GO

/* 유저 고유 ID */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'유저 고유 ID', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Documents', 
	@level2type=N'COLUMN', @level2name=N'uid'
GO

/* 게시글 제목 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'게시글 제목', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Documents', 
	@level2type=N'COLUMN', @level2name=N'doc_title'
GO

/* 내용 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'내용', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Documents', 
	@level2type=N'COLUMN', @level2name=N'content'
GO

/* 올린 시간 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'올린 시간', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Documents', 
	@level2type=N'COLUMN', @level2name=N'post_time'
GO

/* 연결된 파일들 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'연결된 파일들', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Documents', 
	@level2type=N'COLUMN', @level2name=N'file_list'
GO

/* 게시글 기본키 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'게시글 기본키', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Documents', 
	@level2type=N'CONSTRAINT', @level2name=N'PK_Documents'
GO

/* 게시글 기본키 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'게시글 기본키', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Documents', 
	@level2type=N'INDEX', @level2name=N'PK_Documents'
GO

/* 카테고리 */
CREATE TABLE [Category] (
	[cat_id] [int] NOT NULL IDENTITY (1,  1),  /* 카테고리 ID */
	[cat_name] [ntext],  /* 카테고리 이름 */
	[cat_posted] [int] /* 게시글 수 */
)
GO

/* 카테고리 기본키 */
ALTER TABLE [Category]
	ADD
		CONSTRAINT [PK_Category]
		PRIMARY KEY NONCLUSTERED (
			[cat_id] ASC
		)
GO

/* 카테고리 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'카테고리', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Category'
GO

/* 카테고리 ID */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'카테고리 ID', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Category', 
	@level2type=N'COLUMN', @level2name=N'cat_id'
GO

/* 카테고리 이름 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'카테고리 이름', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Category', 
	@level2type=N'COLUMN', @level2name=N'cat_name'
GO

/* 게시글 수 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'게시글 수', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Category', 
	@level2type=N'COLUMN', @level2name=N'cat_posted'
GO

/* 카테고리 기본키 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'카테고리 기본키', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Category', 
	@level2type=N'CONSTRAINT', @level2name=N'PK_Category'
GO

/* 카테고리 기본키 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'카테고리 기본키', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Category', 
	@level2type=N'INDEX', @level2name=N'PK_Category'
GO

/* 로그인 로그 */
CREATE TABLE [login_log] (
	[log_id] [int] NOT NULL IDENTITY (1,  1),  /* 로그순번 */
	[uid] [int],  /* 유저 고유 ID */
	[timestamp] [datetime] NOT NULL,  /* 시간 */
	[address] [text],  /* 시도된 주소 */
	[success] [tinyint] NOT NULL /* 성공여부 */
)
GO

/* 로그인 로그 기본키 */
ALTER TABLE [login_log]
	ADD
		CONSTRAINT [PK_login_log]
		PRIMARY KEY NONCLUSTERED (
			[log_id] ASC
		)
GO

/* 로그인 로그 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'로그인 로그', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'login_log'
GO

/* 로그순번 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'로그순번', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'login_log', 
	@level2type=N'COLUMN', @level2name=N'log_id'
GO

/* 유저 고유 ID */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'유저 고유 ID', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'login_log', 
	@level2type=N'COLUMN', @level2name=N'uid'
GO

/* 시간 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'시간', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'login_log', 
	@level2type=N'COLUMN', @level2name=N'timestamp'
GO

/* 시도된 주소 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'시도된 주소', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'login_log', 
	@level2type=N'COLUMN', @level2name=N'address'
GO

/* 성공여부 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'성공여부', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'login_log', 
	@level2type=N'COLUMN', @level2name=N'success'
GO

/* 로그인 로그 기본키 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'로그인 로그 기본키', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'login_log', 
	@level2type=N'CONSTRAINT', @level2name=N'PK_login_log'
GO

/* 로그인 로그 기본키 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'로그인 로그 기본키', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'login_log', 
	@level2type=N'INDEX', @level2name=N'PK_login_log'
GO

/* Directory */
CREATE TABLE [Directory] (
	[dir_id] [int] NOT NULL IDENTITY (1,  1),  /* 폴더 ID */
	[uid] [int],  /* 유저 고유 ID */
	[name] [ntext] NOT NULL,  /* 폴더 이름 */
	[files_count] [int],  /* 폴더 파일수 */
	[parent_dir] [int] /* 상위 폴더 ID */
)
GO

/* Directory 기본키 */
ALTER TABLE [Directory]
	ADD
		CONSTRAINT [PK_Directory]
		PRIMARY KEY NONCLUSTERED (
			[dir_id] ASC
		)
GO

/* Directory */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'Directory', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Directory'
GO

/* 폴더 ID */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'폴더 ID', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Directory', 
	@level2type=N'COLUMN', @level2name=N'dir_id'
GO

/* 유저 고유 ID */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'유저 고유 ID', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Directory', 
	@level2type=N'COLUMN', @level2name=N'uid'
GO

/* 폴더 이름 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'폴더 이름', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Directory', 
	@level2type=N'COLUMN', @level2name=N'name'
GO

/* 폴더 파일수 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'폴더 파일수', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Directory', 
	@level2type=N'COLUMN', @level2name=N'files_count'
GO

/* 상위 폴더 ID */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'상위 폴더 ID', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Directory', 
	@level2type=N'COLUMN', @level2name=N'parent_dir'
GO

/* Directory 기본키 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'Directory 기본키', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Directory', 
	@level2type=N'CONSTRAINT', @level2name=N'PK_Directory'
GO

/* Directory 기본키 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'Directory 기본키', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Directory', 
	@level2type=N'INDEX', @level2name=N'PK_Directory'
GO

/* 페이스북 토큰 -> 유저 */
ALTER TABLE [Users]
	ADD
		CONSTRAINT [FK_Facebook_Token_TO_Users]
		FOREIGN KEY (
			[token_id]
		)
		REFERENCES [Facebook_Token] (
			[token_id]
		)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
GO

/* 유저 -> 페이스북 토큰 */
ALTER TABLE [Facebook_Token]
	ADD
		CONSTRAINT [FK_Users_TO_Facebook_Token]
		FOREIGN KEY (
			[uid]
		)
		REFERENCES [Users] (
			[uid]
		)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
GO

/* 유저 -> 파일 */
ALTER TABLE [Files]
	ADD
		CONSTRAINT [FK_Users_TO_Files]
		FOREIGN KEY (
			[uid]
		)
		REFERENCES [Users] (
			[uid]
		)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
GO

/* Directory -> 파일 */
ALTER TABLE [Files]
	ADD
		CONSTRAINT [FK_Directory_TO_Files]
		FOREIGN KEY (
			[dir_id]
		)
		REFERENCES [Directory] (
			[dir_id]
		)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
GO

/* 카테고리 -> 게시글 */
ALTER TABLE [Documents]
	ADD
		CONSTRAINT [FK_Category_TO_Documents]
		FOREIGN KEY (
			[cat_id]
		)
		REFERENCES [Category] (
			[cat_id]
		)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
GO

/* 유저 -> 게시글 */
ALTER TABLE [Documents]
	ADD
		CONSTRAINT [FK_Users_TO_Documents]
		FOREIGN KEY (
			[uid]
		)
		REFERENCES [Users] (
			[uid]
		)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
GO

/* 유저 -> 로그인 로그 */
ALTER TABLE [login_log]
	ADD
		CONSTRAINT [FK_Users_TO_login_log]
		FOREIGN KEY (
			[uid]
		)
		REFERENCES [Users] (
			[uid]
		)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
GO

/* Directory -> Directory */
ALTER TABLE [Directory]
	ADD
		CONSTRAINT [FK_Directory_TO_Directory]
		FOREIGN KEY (
			[parent_dir]
		)
		REFERENCES [Directory] (
			[dir_id]
		)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
GO

/* 유저 -> Directory */
ALTER TABLE [Directory]
	ADD
		CONSTRAINT [FK_Users_TO_Directory]
		FOREIGN KEY (
			[uid]
		)
		REFERENCES [Users] (
			[uid]
		)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
GO

/* 페이스북 토큰 -> 유저 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'페이스북 토큰 -> 유저', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Users', 
	@level2type=N'CONSTRAINT', @level2name=N'FK_Facebook_Token_TO_Users'
GO

/* 유저 -> 페이스북 토큰 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'유저 -> 페이스북 토큰', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Facebook_Token', 
	@level2type=N'CONSTRAINT', @level2name=N'FK_Users_TO_Facebook_Token'
GO

/* 유저 -> 파일 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'유저 -> 파일', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Files', 
	@level2type=N'CONSTRAINT', @level2name=N'FK_Users_TO_Files'

/* Directory -> 파일 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'Directory -> 파일', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Files', 
	@level2type=N'CONSTRAINT', @level2name=N'FK_Directory_TO_Files'
GO

/* 카테고리 -> 게시글 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'카테고리 -> 게시글', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Documents', 
	@level2type=N'CONSTRAINT', @level2name=N'FK_Category_TO_Documents'

/* 유저 -> 게시글 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'유저 -> 게시글', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Documents', 
	@level2type=N'CONSTRAINT', @level2name=N'FK_Users_TO_Documents'
GO

/* 유저 -> 로그인 로그 */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'유저 -> 로그인 로그', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'login_log', 
	@level2type=N'CONSTRAINT', @level2name=N'FK_Users_TO_login_log'
GO

/* Directory -> Directory */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'Directory -> Directory', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Directory', 
	@level2type=N'CONSTRAINT', @level2name=N'FK_Directory_TO_Directory'

/* 유저 -> Directory */
EXEC sp_addextendedproperty 
	@name=N'MS_Description', @value=N'유저 -> Directory', 
	@level0type=N'SCHEMA', @level0name=N'Cloud', 
	@level1type=N'TABLE', @level1name=N'Directory', 
	@level2type=N'CONSTRAINT', @level2name=N'FK_Users_TO_Directory'
GO