﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace FileCloud {
	public partial class WebForm1 : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {
			string User_ID = Session["user_id"] == null ? "0" : Session["user_id"].ToString();
			//string Dir_ID = Context.Request.Form["dir_id"];
			string File_ID = Context.Request.QueryString["file_id"];


			bool found = false;
			string file_name = "";
			string physical_file = "";

			SqlManager.Query(String.Format("SELECT * FROM [Files] WHERE  [file_id] = {0}", File_ID),
				new SqlManager.OnRead(delegate (SqlDataReader result) {
					file_name = (string)result["filename"];
					physical_file = (string)result["physical_file"];
					found = true;
				}
			));

			if(found) {
				string[] tmp = file_name.Split('.');
				string mime_type = MimeTypeMap.GetMimeType(tmp[tmp.Length - 1]);

				Context.Response.Clear();
				Context.Response.ContentType = mime_type;
				Context.Response.AddHeader("Content-Disposition", "attachment; filename=" + file_name);
				Context.Response.WriteFile(Server.MapPath("/files/" + physical_file));
				Context.Response.End();
			}
			else {
				Context.Response.StatusCode = 404;
			}
		}
	}
}