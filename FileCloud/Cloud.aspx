﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage1.Master" AutoEventWireup="true" CodeBehind="Cloud.aspx.cs" Inherits="FileCloud.Cloud" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="/css/Cloud.css" type="text/css" />
	<script src="/js/Cloud.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div id="Dropzone" ondragenter="ondragenter1(event)" ondragleave="ondragleave1(event)" ondragover="ondragover1(event)" ondrop="ondrop1(event)" >
	</div>
	<div id="icontext">
		<img src="img/add118.png" style="width: 20%;" /><br />
		파일을 추가하려면 여기에 드롭하세요.
	</div>
	<div id="CloudContext" >
		<div class="list-group">

		</div>
	</div>
</asp:Content>
