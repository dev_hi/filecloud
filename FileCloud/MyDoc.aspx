﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage1.Master" AutoEventWireup="true" CodeBehind="MyDoc.aspx.cs" Inherits="FileCloud.MyDoc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			게시글 기록
		</div>
		<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="doc_id,cat_id,uid" DataSourceID="SqlDataSource1" CssClass="table table-hover table-striped " GridLines="None">
			<Columns>
				<asp:BoundField DataField="doc_id" HeaderText="doc_id" InsertVisible="False" ReadOnly="True" SortExpression="doc_id" />
				<asp:BoundField DataField="cat_id" HeaderText="cat_id" ReadOnly="True" SortExpression="cat_id" />
				<asp:BoundField DataField="uid" HeaderText="uid" ReadOnly="True" SortExpression="uid" />
				<asp:BoundField DataField="doc_title" HeaderText="doc_title" SortExpression="doc_title" />
				<asp:BoundField DataField="content" HeaderText="content" SortExpression="content" />
				<asp:BoundField DataField="post_time" HeaderText="post_time" SortExpression="post_time" />
				<asp:BoundField DataField="file_list" HeaderText="file_list" SortExpression="file_list" />
			</Columns>
		</asp:GridView>
	</div>
	<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CloudConnectionString %>" SelectCommand="SELECT * FROM [Documents] WHERE ([uid] = @uid)">
		<SelectParameters>
			<asp:SessionParameter DefaultValue="1" Name="uid" SessionField="user_id" Type="Int32" />
		</SelectParameters>
	</asp:SqlDataSource>
</asp:Content>
