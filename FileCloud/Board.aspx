﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage1.Master" AutoEventWireup="true" CodeBehind="Board.aspx.cs" Inherits="FileCloud.Board" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">
	  <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource3">
		  <ItemTemplate>
			  <asp:Label ID="cat_nameLabel" runat="server" Text='<%# Bind("cat_name") %>' />
		  </ItemTemplate>
	  </asp:FormView> 게시판</div>
	<asp:GridView ID="GridView1" runat="server" GridLines="None"  CssClass="table table-hover table-striped " AutoGenerateColumns="False" DataKeyNames="doc_id" DataSourceID="SqlDataSource2" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
		<Columns>
			<asp:CommandField ShowSelectButton="True" />
			<asp:BoundField DataField="doc_id" HeaderText="doc_id" InsertVisible="False" ReadOnly="True" SortExpression="doc_id" />
			<asp:BoundField DataField="uid" HeaderText="uid" SortExpression="uid" />
			<asp:BoundField DataField="doc_title" HeaderText="doc_title" SortExpression="doc_title" />
			<asp:BoundField DataField="post_time" HeaderText="post_time" SortExpression="post_time" />
		</Columns>
	</asp:GridView>
		</div>
	<div style="clear:both"></div>
	<asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:CloudConnectionString %>" SelectCommand="SELECT [doc_id], [uid], [doc_title], [post_time] FROM [Documents] WHERE ([cat_id] = @cat_id)">
		<SelectParameters>
			<asp:QueryStringParameter DefaultValue="1" Name="cat_id" QueryStringField="cid" Type="Int32" />
		</SelectParameters>
	</asp:SqlDataSource>
	<asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:CloudConnectionString %>" SelectCommand="SELECT [cat_name] FROM [Category] WHERE ([cat_id] = @cat_id)">
		<SelectParameters>
			<asp:QueryStringParameter DefaultValue="1" Name="cat_id" QueryStringField="cid" Type="Int32" />
		</SelectParameters>
	</asp:SqlDataSource>
</asp:Content>
