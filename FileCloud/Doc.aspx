﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage1.Master" AutoEventWireup="true" CodeBehind="Doc.aspx.cs" Inherits="FileCloud.Doc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

	<asp:FormView ID="FormView1" runat="server" DataKeyNames="doc_id,cat_id,uid" DataSourceID="SqlDataSource1">
		<EditItemTemplate>
			doc_id:
			<asp:Label ID="doc_idLabel1" runat="server" Text='<%# Eval("doc_id") %>' />
			<br />
			cat_id:
			<asp:Label ID="cat_idLabel1" runat="server" Text='<%# Eval("cat_id") %>' />
			<br />
			uid:
			<asp:Label ID="uidLabel1" runat="server" Text='<%# Eval("uid") %>' />
			<br />
			doc_title:
			<asp:TextBox ID="doc_titleTextBox" runat="server" Text='<%# Bind("doc_title") %>' />
			<br />
			content:
			<asp:TextBox ID="contentTextBox" runat="server" Text='<%# Bind("content") %>' />
			<br />
			post_time:
			<asp:TextBox ID="post_timeTextBox" runat="server" Text='<%# Bind("post_time") %>' />
			<br />
			file_list:
			<asp:TextBox ID="file_listTextBox" runat="server" Text='<%# Bind("file_list") %>' />
			<br />
			<asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="업데이트" />
			&nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="취소" />
		</EditItemTemplate>
		<InsertItemTemplate>
			cat_id:
			<asp:TextBox ID="cat_idTextBox" runat="server" Text='<%# Bind("cat_id") %>' />
			<br />
			uid:
			<asp:TextBox ID="uidTextBox" runat="server" Text='<%# Bind("uid") %>' />
			<br />
			doc_title:
			<asp:TextBox ID="doc_titleTextBox" runat="server" Text='<%# Bind("doc_title") %>' />
			<br />
			content:
			<asp:TextBox ID="contentTextBox" runat="server" Text='<%# Bind("content") %>' />
			<br />
			post_time:
			<asp:TextBox ID="post_timeTextBox" runat="server" Text='<%# Bind("post_time") %>' />
			<br />
			file_list:
			<asp:TextBox ID="file_listTextBox" runat="server" Text='<%# Bind("file_list") %>' />
			<br />
			<asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="삽입" />
			&nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="취소" />
		</InsertItemTemplate>
		<ItemTemplate>
			<div class="jumbotron">
				<h1><asp:Label ID="doc_titleLabel" runat="server" Text='<%# Bind("doc_title") %>' /></h1>
				<h3>
					작성일시 : <asp:Label ID="post_timeLabel" runat="server" Text='<%# Bind("post_time") %>' />
					<asp:FormView ID="FormView2" runat="server" DataSourceID="SqlDataSource1">
						<EditItemTemplate>
							username:
							<asp:TextBox ID="usernameTextBox" runat="server" Text='<%# Bind("username") %>' />
							<br />
							<asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="업데이트" />
							&nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="취소" />
						</EditItemTemplate>
						<InsertItemTemplate>
							username:
							<asp:TextBox ID="usernameTextBox" runat="server" Text='<%# Bind("username") %>' />
							<br />
							<asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="삽입" />
							&nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="취소" />
						</InsertItemTemplate>
						<ItemTemplate>
							username:
							<asp:Label ID="usernameLabel" runat="server" Text='<%# Bind("username") %>' />
							<br />
						</ItemTemplate>
					</asp:FormView>
					<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CloudConnectionString %>" SelectCommand='<%# "SELECT [username] FROM [Users] WHERE ([uid] = " + Eval("uid") + ")" %>'>
						<SelectParameters>
							<asp:Parameter DefaultValue="1" Name="uid" Type="Int32" />
						</SelectParameters>
					</asp:SqlDataSource>
				</h3>
				<div class="container">
					<asp:Label ID="contentLabel" runat="server" Text='<%# Bind("content") %>' />
				</div>
			</div>

		</ItemTemplate>
	</asp:FormView>
	<div>

		<asp:Label ID="Label1" runat="server" Text=""></asp:Label>

	</div>
	<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CloudConnectionString %>" SelectCommand="SELECT * FROM [Documents] WHERE ([doc_id] = @doc_id)">
		<SelectParameters>
			<asp:QueryStringParameter DefaultValue="1" Name="doc_id" QueryStringField="doc_id" Type="Int32" />
		</SelectParameters>
	</asp:SqlDataSource>

</asp:Content>
