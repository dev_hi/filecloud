﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

namespace FileCloud {
	public partial class Status : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {
			Label1.Text = "현재 방문자 수 : " + Application["cur_count"];
			Label2.Text = "총 방문자 수 : " + Application["count"];
            userListBox.Items.Clear();
			ArrayList userlist = (ArrayList)Application.Get("userlist");

			foreach(string list in userlist) {
				userListBox.Items.Add(list);
			}
		}
	}
}