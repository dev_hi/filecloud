﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage1.Master" AutoEventWireup="true" CodeBehind="CreateDoc.aspx.cs" Inherits="FileCloud.CreateDoc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<h1>제목</h1>
	<div class="input-group">
	<asp:TextBox ID="TextBox1" runat="server" CssClass="form-control"></asp:TextBox>
	</div>
	<h1>내용</h1>
	<div class="input-group">
	<br />
	<asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" TextMode="MultiLine" Height="400px"></asp:TextBox>
	</div>
	<h1>게시판</h1>
	<asp:DropDownList ID="DropDownList1" runat="server" Width="300px" DataSourceID="SqlDataSource1" DataTextField="cat_name" DataValueField="cat_id"></asp:DropDownList>
	<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CloudConnectionString %>" SelectCommand="SELECT [cat_name], [cat_id] FROM [Category]"></asp:SqlDataSource>
	<br />
	<br />
	<h1>첨부</h1>
	<asp:ListBox ID="ListBox1" runat="server" Width="400px" Height="400px" AutoPostBack="True" DataSourceID="SqlDataSource2" DataTextField="filename" DataValueField="file_id" OnSelectedIndexChanged="ListBox1_SelectedIndexChanged"></asp:ListBox>
	<asp:ListBox ID="ListBox2" runat="server" Width="400px" Height="400px" AutoPostBack="True" OnSelectedIndexChanged="ListBox2_SelectedIndexChanged"></asp:ListBox>
	<asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:CloudConnectionString %>" SelectCommand="SELECT * FROM [Files] WHERE ([uid] = @uid)">
		<SelectParameters>
			<asp:SessionParameter DefaultValue="1" Name="uid" SessionField="user_id" Type="Int32" />
		</SelectParameters>
	</asp:SqlDataSource>
	<br />
	<br />
	<asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="제출" CssClass="btn btn-success" />
	<asp:Button ID="Button2" runat="server" Text="취소"  CssClass="btn btn-danger" OnClick="Button2_Click"/>
</asp:Content>
