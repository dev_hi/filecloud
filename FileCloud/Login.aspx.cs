﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Xml;

namespace FileCloud
{
	public partial class Login : System.Web.UI.Page
	{
        private SqlConnection scon;
		protected void Page_Load(object sender, EventArgs e)
		{
			
		}

		protected void Button2_Click(object sender, EventArgs e) {

			//회원가입 버튼
			Response.Redirect("Register.aspx");
		}

		protected void Button1_Click(object sender, EventArgs e) {
			try {
				XmlDocument xml = new XmlDocument();
				xml.Load(Server.MapPath("/Settings.xml"));
				XmlNode root = xml["settings"]["database"];
				//SqlConnection server = new SqlConnection(String.Format("server={0}\\{1};Integrated Security={2};database={3};", root["server"], root["instance"], root["integrated_security"], root["scheme"]));

				//string ConStr = "Data Source=JK-PC\\sqlexpress;Initial Catalog=Cloud;Integrated Security=True";
				string ConStr = String.Format("server={0}\\{1};Integrated Security={2};database={3};", root["server"].InnerText, root["instance"].InnerText, root["integrated_security"].InnerText, root["scheme"].InnerText);
				scon = new SqlConnection(ConStr);
				scon.Open();

				if(username.Value == "" || password.Value == "") {
					Response.Write("<script type='text/javascript'>alert('ID 또는 PW를 입력하세요.')</script>");
				}
				else {
					string loginQuery = "SELECT * FROM Users WHERE username LIKE '" + username.Value + "' AND PWDCOMPARE('" + password.Value + "',password)=1";

					SqlCommand scmd = new SqlCommand(loginQuery, scon);
					scmd.ExecuteNonQuery();
					SqlDataReader objDr = scmd.ExecuteReader();

					if(objDr.Read()) {

						SqlManager.Query("INSERT INTO [login_log] ([uid], [timestamp], [address], [success]) VALUES (" + (int)objDr["uid"] + ", CURRENT_TIMESTAMP, '" + Request.ServerVariables["REMOTE_ADDR"] + "', 1)");

						Session["login_success"] = true;
						Session["user_id"] = (int)objDr["uid"];
						Session["user_level"] = objDr["level"].ToString();
                        Session["username"] = objDr["username"].ToString();    //★나중에 로그아웃되면 Session.Remove 해야함.
						//Response.Write("<script type='text/javascript'>alert('" + Session["username"].ToString() + "님 로그인되었습니다.')</script>");

						username.Style.Add("background-color", "#B7F0B1");
						password.Style.Add("background-color", "#B7F0B1");
						System.Collections.ArrayList userlist = (System.Collections.ArrayList)Application.Get("userlist");
						if(!userlist.Contains(username.Value)) userlist.Add(username.Value);
						Response.Redirect("Cloud.aspx");
					}
					else {
						bool found = false;
						SqlManager.Query("SELECT [uid] FROM Users WHERE username LIKE '" + username.Value, new SqlManager.OnRead(delegate (SqlDataReader result) {
							if(!found) {
								SqlManager.Query("INSERT INTO [login_log] ([uid], [timestamp], [address], [success]) VALUES (" + (int)result["uid"] + ", CURRENT_TIMESTAMP, '" + Request.ServerVariables["REMOTE_ADDR"] + "', 0)");
								found = true;
							}
						}));
						//Response.Write("<script type='text/javascript'>alert('ID 또는 PW가 틀립니다.')</script>");

						username.Style.Add("background-color", "#FFA7A7");
						password.Style.Add("background-color", "#FFA7A7");
					}
					scon.Close();
				}
			}
			catch(Exception ex) {
				username.Style.Add("background-color", "#FAED7D");
				password.Style.Add("background-color", "#FAED7D");
				//Response.Write("<script type='text/javascript'>alert('로그인 실패 : " + ex.Message + "')</script>");
			}
		}
	}
}