﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage1.Master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="FileCloud.index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div id="home_welcome">
		<img src="/img/cloud189.png" width="40%"/>
		<h1>
			클라우드에 파일 업로드
		</h1>
		<p class="lead">
			파일을 업로드 하거나 배포하려면 로그인 하세요.
		</p>
		<asp:Button ID="Button1" runat="server" Text="로그인" CssClass="btn btn-default" PostBackUrl="~/Login.aspx" /><br /><br />
		<asp:Button ID="Button2" runat="server" Text="회원가입" CssClass="btn btn-info" PostBackUrl="~/Register.aspx" />
	</div>
</asp:Content>
