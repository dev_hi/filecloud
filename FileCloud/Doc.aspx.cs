﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace FileCloud {
	public partial class Doc : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {
			if(Request.QueryString["doc_id"] == null) {
				Response.Redirect("/index.aspx");
			}
			else {
				SqlManager.Query("SELECT [file_list] FROM [Documents] WHERE [doc_id] = " + Request.QueryString["doc_id"],
					new SqlManager.OnRead(delegate (SqlDataReader result) {
						if(!result.IsDBNull(0)) {
							string files = (string)result[0];
							string[] tmp = files.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
							int i = 0;
							string qq = "";
							foreach(string f in tmp) {
								qq += "[file_id] = " + f;
								if(i != tmp.Length - 1) {
									qq += " OR ";
								}
								i++;
							}

							string sql = "SELECT * FROM [Files] WHERE " + qq;
							string html = "";

							SqlManager.Query(sql, new SqlManager.OnRead(delegate (SqlDataReader result2) {
								html += String.Format("<a href='/GetFile.aspx?file_id={0}' target='_blank'>{1} 받기</a><br>", result2["file_id"], result2["filename"]);
                            }));

							Label1.Text = html;
						}
                    }));
			}
		}
	}
}