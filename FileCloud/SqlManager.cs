﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Data.SqlClient;

namespace FileCloud {
	public class SqlManager {
		public delegate void OnRead(SqlDataReader reader);

		public static int Query(string sql) {
			XmlDocument xml = new XmlDocument();
			xml.Load(HttpContext.Current.Server.MapPath("/Settings.xml"));
			XmlNode root = xml["settings"]["database"];
			SqlConnection server = new SqlConnection(String.Format("server={0}\\{1};Integrated Security={2};database={3};", root["server"].InnerText, root["instance"].InnerText, root["integrated_security"].InnerText, root["scheme"].InnerText));

			server.Open();
			SqlCommand query = new SqlCommand(sql, server);
			int ret = query.ExecuteNonQuery();
			server.Close();
			return ret;
		}

		public static void Query(string sql, OnRead onread) {

			XmlDocument xml = new XmlDocument();
			xml.Load(HttpContext.Current.Server.MapPath("/Settings.xml"));
			XmlNode root = xml["settings"]["database"];
			SqlConnection server = new SqlConnection(String.Format("server={0}\\{1};Integrated Security={2};database={3};", root["server"].InnerText, root["instance"].InnerText, root["integrated_security"].InnerText, root["scheme"].InnerText));

			server.Open();
			SqlCommand query = new SqlCommand(sql, server);
			SqlDataReader reader = query.ExecuteReader();
			while(reader.Read()) {
				onread(reader);
			}
			server.Close();
		}
	}
}