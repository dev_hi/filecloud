﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FileCloud {
	public partial class CreateDoc : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {
			Session["login_success"] = Session["login_success"] == null ? false : Session["login_success"];
			if(!((bool)Session["login_success"]))
				Response.Redirect("/index.aspx");
		}

		protected void Button1_Click(object sender, EventArgs e) {
			string files = "/";
			foreach(ListItem item in ListBox2.Items) {
				files += item.Value + "/";
            }
			string ff = "INSERT INTO [Documents] ([cat_id], [uid], [doc_title], [content], [post_time], [file_list]) VALUES ({0},{1},'{2}','{3}',CURRENT_TIMESTAMP,'{4}')";
            string sql = String.Format(ff,DropDownList1.SelectedValue,(int)Session["user_id"],TextBox1.Text,TextBox2.Text.Replace("\r\n", "<br>"),files);
			
			if(SqlManager.Query(sql) > 0) {
				Response.Redirect("/Board.aspx?cid=" + DropDownList1.SelectedValue);
			}
		}

		protected void Button2_Click(object sender, EventArgs e) {
			Response.Redirect("/index.aspx");
		}

		protected void ListBox1_SelectedIndexChanged(object sender, EventArgs e) {
			bool found = false;
			foreach(ListItem item in ListBox2.Items) {
				if(item.Value == ListBox1.SelectedValue)
					found = true;
			}
			if(!found) {
				ListBox2.Items.Add(new ListItem(ListBox1.SelectedItem.Text, ListBox1.SelectedItem.Value));
			}
		}

		protected void ListBox2_SelectedIndexChanged(object sender, EventArgs e) {
			ListBox2.Items.Remove(ListBox2.SelectedItem);
		}
	}
}