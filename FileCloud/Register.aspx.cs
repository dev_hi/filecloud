﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Xml;

namespace FileCloud {
	public partial class Register : System.Web.UI.Page {
		private SqlConnection scon;
		protected void Page_Load(object sender, EventArgs e) {

		}

		protected void Button1_Click(object sender, EventArgs e) {
			//제출 버튼 (회원가입)
			try {
				XmlDocument xml = new XmlDocument();
				xml.Load(Server.MapPath("/Settings.xml"));
				XmlNode root = xml["settings"]["database"];
				//SqlConnection server = new SqlConnection(String.Format("server={0}\\{1};Integrated Security={2};database={3};", root["server"], root["instance"], root["integrated_security"], root["scheme"]));

				//string ConStr = "Data Source=JK-PC\\sqlexpress;Initial Catalog=Cloud;Integrated Security=True";
				string ConStr = String.Format("server={0}\\{1};Integrated Security={2};database={3};", root["server"].InnerText, root["instance"].InnerText, root["integrated_security"].InnerText, root["scheme"].InnerText);
				scon = new SqlConnection(ConStr);
				scon.Open();

				if(Password.Text != "" && (Password.Text == Password_confirm.Text)) {
					string joinQuery = "INSERT INTO Users(username,password,age,gender) VALUES('" +
						username.Text + "',pwdencrypt('" + Password.Text + "')," + int.Parse(age.Text) + "," + int.Parse(gender.SelectedValue) + ")";

					SqlCommand scmd = new SqlCommand(joinQuery, scon);
					scmd.ExecuteNonQuery();
					scon.Close();

					Response.Write("<script type='text/javascript'>alert('회원가입이 완료되었습니다.')</script>");

					//Session["id"] = username.Text;
					Response.Redirect("index.aspx");
				}
			}
			catch(Exception ex) {
				Response.Write("<script type='text/javascript'>alert('회원가입 실패 : " + ex.Message + "')</script>");
			}
		}

		protected void Button2_Click(object sender, EventArgs e) {
			//취소 버튼
			Response.Redirect("index.aspx");
		}
	}
}