﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.IO;
using System.Text;

namespace FileCloud {
	/// <summary>
	/// ajax의 요약 설명입니다.
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// ASP.NET AJAX를 사용하여 스크립트에서 이 웹 서비스를 호출하려면 다음 줄의 주석 처리를 제거합니다. 
	[System.Web.Script.Services.ScriptService]
	public class ajax : System.Web.Services.WebService {

		[WebMethod(EnableSession = true)]
		public void TestMethod() {
			JsonObject obj = new JsonObject();
			string uid = Session["user_id"] == null ? "0" : Session["user_id"].ToString();
			obj.AddNode(new JsonObject("session_uid", uid, JsonObjectType.@string));
			JsonObject array = new JsonObject("POST_values", JsonObjectType.@object);
			foreach(string key in Context.Request.Form.Keys) {
				array.AddNode(new JsonObject(key, Context.Request.Form[key], JsonObjectType.@string));
			}
			obj.AddNode(array);

			Context.Response.Clear();
			Context.Response.ContentType = "application/json";
			Context.Response.Write(obj.ToString());
			Context.Response.End();
		}

		[WebMethod(EnableSession = true)]
		public void UploadFile() {
			string uid = Session["user_id"] == null ? "0" : Session["user_id"].ToString();
			JsonObject response = new JsonObject();
			HttpPostedFile POST_File = Context.Request.Files[0];
			string filename = POST_File.FileName;
			if(filename.IndexOf('\\') > -1) {
				string[] tmp = filename.Split('\\');
				filename = tmp[tmp.Length - 1];
			}
			int filesize = POST_File.ContentLength;
			string targetpath = Context.Request.Form["dir_id"] == null ? "NULL" : Context.Request.Form["dir_id"];
			MD5 md5 = MD5.Create();
			md5.Initialize();
			byte[] hashbyte = md5.ComputeHash(POST_File.InputStream);
			StringBuilder sb = new StringBuilder();
			foreach(byte h in hashbyte) {
				sb.Append(h.ToString("X2"));
			}
			string hash = sb.ToString();
			Guid Guid = Guid.NewGuid();
			string physical_name = Guid.ToString("N");

			bool found = false;
			SqlManager.Query("SELECT [filename] FROM [Files] WHERE [dir_id] = " + targetpath,
				new SqlManager.OnRead(delegate (SqlDataReader result) {
					if(result["filename"].ToString() == filename) found = true;
				}
			));

			if(!found) {
				int affected_rows = SqlManager.Query(
					String.Format(
						"INSERT INTO Files ([uid], [dir_id], [filename], [filesize], [hash], [upload_time], [physical_file]) VALUES ({0},{1},'{2}',{3},'{4}',CURRENT_TIMESTAMP,'{5}')",
						uid, targetpath, filename, filesize, hash, physical_name
					)
				);

				if(affected_rows > 0) {
					response.AddNode(new JsonObject("code", 1, JsonObjectType.number));
					response.AddNode(new JsonObject("message", "업로드에 성공했습니다.", JsonObjectType.@string));
					POST_File.SaveAs(Server.MapPath("/files") + "\\" + physical_name);
					if(targetpath != "NULL") SqlManager.Query("UPDATE [Directory] SET [files_count] = [files_count] + 1 WHERE [dir_id] = " + targetpath);
				}
				else {
					response.AddNode(new JsonObject("code", 2, JsonObjectType.number));
					response.AddNode(new JsonObject("message", "업로드에 실패했습니다.", JsonObjectType.@string));
					response.AddNode(new JsonObject("reason", "SQL에러", JsonObjectType.@string));
				}
			}
			else {
				response.AddNode(new JsonObject("code", 2, JsonObjectType.number));
				response.AddNode(new JsonObject("message", "업로드에 실패했습니다.", JsonObjectType.@string));
				response.AddNode(new JsonObject("reason", "이미 파일이 존재합니다.", JsonObjectType.@string));
			}

			Context.Response.Clear();
			Context.Response.ContentType = "application/json";
			Context.Response.Write(response.ToString());
			Context.Response.End();
		}

		[WebMethod(EnableSession = true)]
		public void GetFile() {
			string User_ID = Session["user_id"] == null ? "0" : Session["user_id"].ToString();
			string Dir_ID = Context.Request.QueryString["dir_id"] == null ? "IS NULL" : "= " + Context.Request.QueryString["dir_id"];
			//string Dir_ID = Context.Request.Form["dir_id"];
			string File_ID = Context.Request.QueryString["file_id"];


			bool found = false;
			string file_name = "";
			string physical_file = "";

			SqlManager.Query(String.Format("SELECT * FROM [Files] WHERE [uid] = {0} AND [file_id] = {1} AND [dir_id] {2}", User_ID, File_ID, Dir_ID),
				new SqlManager.OnRead(delegate (SqlDataReader result) {
					file_name = (string)result["filename"];
					physical_file = (string)result["physical_file"];
					found = true;
				}
			));

			if(found) {
				string[] tmp = file_name.Split('.');
				string mime_type = MimeTypeMap.GetMimeType(tmp[tmp.Length - 1]);

				Context.Response.Clear();
				Context.Response.ContentType = mime_type;
				Context.Response.AddHeader("Content-Disposition", "attachment; filename=" + file_name);
				Context.Response.WriteFile(Server.MapPath("/files/" + physical_file));
				Context.Response.End();
			}
			else {
				Context.Response.StatusCode = 404;
			}


		}

		[WebMethod(EnableSession = true)]
		public void GetList() {
			string User_ID = Session["user_id"] == null ? "0" : Session["user_id"].ToString();
			string Dir_ID = Context.Request.Form["dir_id"] == null || Context.Request.Form["dir_id"] == "0" ? "IS NULL" : "= " + Context.Request.Form["dir_id"];
			//string Dir_ID = Context.Request.Form["dir_id"] == null ? "0" : Context.Request.Form["dir_id"];

			JsonObject response = new JsonObject();
			response.AddNode(new JsonObject("session_uid", int.Parse(User_ID), JsonObjectType.number));
			response.AddNode(new JsonObject("code", 1, JsonObjectType.number));
			response.AddNode(new JsonObject("message", "목록 호출에 성공했습니다.", JsonObjectType.@string));
			SqlManager.Query(String.Format("SELECT [parent_dir] FROM [Directory] WHERE [uid] = {0} AND [dir_id] {1}", User_ID, Dir_ID),
				new SqlManager.OnRead(delegate (SqlDataReader result) {
					if(result.IsDBNull(0))
						response.AddNode(new JsonObject("parent_dir", null, JsonObjectType.@null));
					else 
						response.AddNode(new JsonObject("parent_dir", (int)result["parent_dir"], JsonObjectType.number));
				}
			));
			JsonObject item_list = new JsonObject("list",JsonObjectType.array);

			SqlManager.Query(String.Format("SELECT * FROM [Directory] WHERE [uid] = {0} AND [parent_dir] {1}", User_ID, Dir_ID),
				new SqlManager.OnRead(delegate (SqlDataReader result) {
					int dir_id = (int)result["dir_id"];
					int files_count = (int)result["files_count"];
					string name = (string)result["name"];
					JsonObject dir_info = new JsonObject();
					dir_info.AddNode(new JsonObject("type", "dir", JsonObjectType.@string));
					dir_info.AddNode(new JsonObject("name", name, JsonObjectType.@string));
					dir_info.AddNode(new JsonObject("dir_id", (dir_id), JsonObjectType.number));
					dir_info.AddNode(new JsonObject("files_count", (files_count), JsonObjectType.number));

					item_list.AddNode(dir_info);
				}
			));

			SqlManager.Query(String.Format("SELECT * FROM [Files] WHERE [uid] = {0} AND [dir_id] {1}", User_ID, Dir_ID),
				new SqlManager.OnRead(delegate (SqlDataReader result) {
					string file_name = (string)result["filename"];
					int file_id = (int)result["file_id"];
					int filesize = (int)result["filesize"];
					DateTime upload_time = (DateTime)result["upload_time"];
					string[] tmp = file_name.Split('.');
					string mime_type = MimeTypeMap.GetMimeType(tmp[tmp.Length - 1]);

					JsonObject file_info = new JsonObject();
					file_info.AddNode(new JsonObject("type", mime_type, JsonObjectType.@string));
					file_info.AddNode(new JsonObject("name", file_name, JsonObjectType.@string));
					file_info.AddNode(new JsonObject("file_id", (file_id), JsonObjectType.number));
					file_info.AddNode(new JsonObject("size", (filesize), JsonObjectType.number));
					file_info.AddNode(new JsonObject("upload_time", upload_time.Subtract(new DateTime(1970, 1, 1)).TotalSeconds, JsonObjectType.number));

					item_list.AddNode(file_info);
				}
			));

			response.AddNode(item_list);

			Context.Response.Clear();
			Context.Response.ContentType = "application/json";
			Context.Response.Write(response.ToString());
			Context.Response.End();

		}

		[WebMethod(EnableSession = true)]
		public void CreateDirectory() {
			string User_ID = Session["user_id"] == null ? "0" : Session["user_id"].ToString();
			string Dir_ID = Context.Request.Form["parent_id"] == null ? "NULL" : Context.Request.Form["parent_id"];
			string Dir_name = Context.Request.Form["dir_name"];
			JsonObject response = new JsonObject();

			string sql_text = "SELECT * FROM [Directory] WHERE [uid] = " + User_ID + " AND [parent_dir] " + (Context.Request.Form["parent_id"] == null ? "IS" : "=") + " " + Dir_ID;
			bool exist = false;
			SqlManager.Query(sql_text,
				new SqlManager.OnRead(delegate (SqlDataReader result) {
					if(result["name"].ToString() == Dir_name) exist = true;
				})
			);
			if(exist) {
				response.AddNode(new JsonObject("session_uid", int.Parse(User_ID), JsonObjectType.number));
				response.AddNode(new JsonObject("code", 2, JsonObjectType.number));
				response.AddNode(new JsonObject("message", "폴더 생성에 실패했습니다.", JsonObjectType.@string));
				response.AddNode(new JsonObject("reason", "이미 존재하는 폴더입니다.", JsonObjectType.@string));
			}
			else {
				sql_text = String.Format("INSERT INTO [Directory] ([uid], [name], [files_count], [parent_dir]) VALUES ({0},'{1}',{2},{3})", User_ID, Dir_name, 0, Dir_ID);
				if(SqlManager.Query(sql_text) > 0) {
					response.AddNode(new JsonObject("session_uid", int.Parse(User_ID), JsonObjectType.number));
					response.AddNode(new JsonObject("code", 1, JsonObjectType.number));
					response.AddNode(new JsonObject("message", "폴더 생성에 성공했습니다.", JsonObjectType.@string));
					if(Dir_ID != "NULL") SqlManager.Query("UPDATE [Directory] SET [files_count] = [files_count] + 1 WHERE [dir_id] = " + Dir_ID);
				}
				else {
					response.AddNode(new JsonObject("session_uid", int.Parse(User_ID), JsonObjectType.number));
					response.AddNode(new JsonObject("code", 2, JsonObjectType.number));
					response.AddNode(new JsonObject("message", "업로드에 실패했습니다.", JsonObjectType.@string));
					response.AddNode(new JsonObject("reason", "SQL에러", JsonObjectType.@string));
				}
			}

			Context.Response.Clear();
			Context.Response.ContentType = "application/json";
			Context.Response.Write(response.ToString());
			Context.Response.End();
		}

		[WebMethod(EnableSession = true)]
		public void DeleteItem() {
			string session_user_id = Session["user_id"] == null ? "0" : Session["user_id"].ToString();
			string target_id = Context.Request.Form["target_id"] == null ? "NULL" : Context.Request.Form["target_id"];
			string isDir = Context.Request.Form["isDir"] == null ? "false" : Context.Request.Form["isDir"];

			JsonObject response = new JsonObject();

			try {
				if(isDir == "false") {
					SqlManager.Query("SELECT [file_id], [physical_file] FROM [Files] WHERE [file_id] = " + target_id + " AND [uid] = " + session_user_id,
						new SqlManager.OnRead(delegate (SqlDataReader result) {
							File.Delete(Server.MapPath("/files/" + result["physical_file"]));
							SqlManager.Query("DELETE FROM [Files] WHERE [file_id] = " + result["file_id"]);
						}
					));
				}
				else {
					DeleteDirectory(session_user_id, target_id);
				}
				response.AddNode(new JsonObject("session_uid", session_user_id, JsonObjectType.number));
				response.AddNode(new JsonObject("code", 1, JsonObjectType.number));
				response.AddNode(new JsonObject("message", "삭제에 성공했습니다.", JsonObjectType.@string));
			}
			catch (Exception ex) {
				response.AddNode(new JsonObject("session_uid", session_user_id, JsonObjectType.number));
				response.AddNode(new JsonObject("code", 2, JsonObjectType.number));
				response.AddNode(new JsonObject("message", "삭제에 실패했습니다.", JsonObjectType.@string));
				response.AddNode(new JsonObject("reason", ex.Message, JsonObjectType.@string));
			}

			Context.Response.Clear();
			Context.Response.ContentType = "application/json";
			Context.Response.Write(response.ToString());
			Context.Response.End();
		}

		public void DeleteDirectory(string uid, string id) {
			SqlManager.Query("SELECT [dir_id] FROM [Directory] WHERE [parent_dir] = " + id + " AND [uid] = " + uid,
				new SqlManager.OnRead(delegate (SqlDataReader result) {
					DeleteDirectory(uid, result["dir_id"].ToString());
				}
			));
			SqlManager.Query("SELECT [file_id], [physical_file] FROM [Files] WHERE [dir_id] = " + id + " AND [uid] = " + uid,
				new SqlManager.OnRead(delegate (SqlDataReader result) {
					File.Delete(Server.MapPath("/files/" + result["physical_file"]));
					SqlManager.Query("DELETE FROM [Files] WHERE [file_id] = " + result["file_id"]);
				}
			));
			SqlManager.Query("DELETE FROM [Directory] WHERE [dir_id] = " + id);
		}

		public bool IsReuseable {
			get {
				return false;
			}
		}
	}
}
