﻿var current_dir_id = undefined;
var current_context_menu = undefined;
function v(a) { return !(typeof a == "undefined"); }
function getAllEvents(element) {
	var result = [];
	var except = ["dragenter", "dragover", "dragleave", "drop"];
	for (var key in element) {
		if (key.indexOf('on') === 0) {
			if (except.indexOf(key.slice(2)) == -1) {
				result.push(key.slice(2));
			}
		}
	}
	return result.join(' ');
}
$("html").ready(function () {
	document.oncontextmenu = function () { return false; };
	var dropzone = $("#Dropzone");
	dropzone.bind(getAllEvents(dropzone[0]), function (e) {
		setTimeout(function () { dropzone.css("pointer-events", "none"); }, 100);
	});
	$("html").bind("dragenter", function (e) {
		e.preventDefault();
		dropzone.css("pointer-events", "all");
	});
	$("html").bind("dragover", function (e) {
		e.preventDefault();
		dropzone.css("pointer-events", "all");
	});
	$("html").bind("dragleave", function (e) {
		e.preventDefault();
		dropzone.css("pointer-events", "all");
	});
	$("html").bind("drop", function (e) {
		e.preventDefault();
		dropzone.css("pointer-events", "all");
	});
	getlist();
});

function ondragenter1(e) {
	$("#icontext").show();
	$("#icontext").animate({ opacity: 0.7 });
	$("#Dropzone").animate({ opacity: 0.7 });
	e.preventDefault();
	console.log(e);
	
}

function ondragleave1(e) {
	$("#Dropzone").animate({ opacity: 0 });
	$("#icontext").animate({ opacity: 0 });
	$("#icontext").hide();
	e.preventDefault();
	console.log(e);
	
}

function ondragover1(e) {
	e.preventDefault();
}

function ondrop1(e) {
	if (e.dataTransfer != null) {
		if (e.dataTransfer.files.length == 0) {
			alert("파일을 올려주세요");
		}
		else if (e.dataTransfer.files.length > 1) {
			alert("파일을 한개씩 올려주세요");
		}
		else if (e.dataTransfer.files.length == 1) {
			$("#Dropzone").animate({ opacity: 0 });
			$("#icontext").animate({ opacity: 0 });
			$("#icontext").hide();
			e.preventDefault();
			e.stopPropagation();

			console.log(e);

			upload(e.dataTransfer.files[0]);
		}
	}
	else {
		alert("파일을 올려주세요");
	}
}

function upload(file) {
	var data = new FormData();
	data.append("upload_file", file);
	if (v(current_dir_id))
		data.append("dir_id", current_dir_id);

	$.ajax({
		url: '/ajax.asmx/UploadFile',
		method: 'POST',
		data: data,
		processData: false,
		contentType: false
	}).done(function (data) {
		if (data.code == 1) {
			getlist(current_dir_id);
		}
		else {
			alert(data.message + "\n코드 : " + data.code);
		}
		console.log(data);
	}).fail(function () {
		alert("서버와 통신이 되지않습니다.");
	});
}

function getlist(dir_id) {
	dir_id = dir_id == "undefined" ? undefined : dir_id;
	var data = new FormData();
	if (v(dir_id)) {
		data.append("dir_id", dir_id);
		current_dir_id = dir_id;
	}
	else {
		current_dir_id = undefined;
	}

	$.ajax({
		url: '/ajax.asmx/GetList',
		method: 'POST',
		data: data,
		processData: false,
		contentType: false
	}).done(function (data) {
		//current_dir_id = dir_id;
		if (data.code == 1) {
			var Context = $("#CloudContext");
			var ListView = Context.find("div.list-group");
			ListView.html("");

			if (v(dir_id)) {

				var up = $("<a class='list-group-item' data-id='" + data.parent_dir + "' data-isDir='true'>" +
					"<h4 class='list-group-item-heading'><span class='glyphicon glyphicon-folder-open'></span>&nbsp;상위폴더로...</h4>" +
					"</a>");
				up.mousedown(function (e) {
					var target = $(e.currentTarget);
					var file_id = target.attr("data-id");
					var name = target.find(".list-group-item-heading").html();
					if (e.button == 0) {
						if (!CancelContextMenu())
							if (!target.hasClass("active")) {
								target.parent().children().removeClass("active");
								target.addClass("active");
							}
					}
					else if (e.button == 2) {
						if (!target.hasClass("active")) {
							target.parent().children().removeClass("active");
							target.addClass("active");
						}
						ShowContextMenu(e.pageX, e.pageY, file_id, name, true);
					}

					console.log(e.button, e.buttons);
					e.preventDefault();
					return false;
				});
				up.dblclick(function (e) {
					var target = $(e.currentTarget);
					var id = target.attr("data-id");
					console.log(id);
					if (id == "null") getlist();
					else getlist(id);
					e.preventDefault();
					return false;
				});
				up.appendTo(ListView);
				up.hide();
				up.slideDown();
			}

			data.list.forEach(function (val) {
				var isDir = val.type == "dir";
				var item = $("<a class='list-group-item' data-id='" + (isDir ? val.dir_id : val.file_id) + "' data-isDir='" + (isDir ? "true" : "false") + "'>" +
					"<h4 class='list-group-item-heading'><span class='glyphicon glyphicon-" + (isDir ? "folder-open" : "file") + "'></span>&nbsp;" + val.name + "</h4>" +
					"<p class='list-group-item-text'>" + (isDir ? "파일수 : " + val.files_count + "개" : "size : " + val.size + "byte") + "</p></a>");
				item.mousedown(function (e) {
					var target = $(e.currentTarget);
					var file_id = target.attr("data-id");
					var name = target.find(".list-group-item-heading").html();
					if (e.button == 0) {
						if(!CancelContextMenu())
							if (!target.hasClass("active")) {
								target.parent().children().removeClass("active");
								target.addClass("active");
							}
					}
					else if (e.button == 2) {
						if (!target.hasClass("active")) {
							target.parent().children().removeClass("active");
							target.addClass("active");
						}
						ShowContextMenu(e.pageX, e.pageY, file_id, name, isDir);
					}

					console.log(e.button, e.buttons);
					e.preventDefault();
					return false;
				});
				item.dblclick(function (e) {
					var target = $(e.currentTarget);
					var id = target.attr("data-id");
					var isDir = target.attr("data-isDir");
					if (isDir == "true") {
						console.log(id);
						getlist(id);
					}
					else {
						window.open("/GetFile.aspx?file_id=" + id, "_blank");
					}
					e.preventDefault();
					return false;
				});
				console.log(item); 
				item.appendTo(ListView);
				item.hide();
				item.slideDown();
				
			});
		}
		else {

		}

		console.log(data);
	}).fail(function () {
		alert("서버와 통신이 되지않습니다.");
	});
}


function ShowContextMenu(x, y, target, name, isdir) {
	CancelContextMenu();
	var new_menu = $("<ul class='ContextMenu' data-id='" + target + "' data-isDir='" + (isdir ? "true" : "false") +"'></ul>");
	var file = $("<li class='MenuItem'>" + name + " " + (isdir ? "열기" : "다운로드") + "</li>");
	file.click(function (e) {
		CancelContextMenu();
		var target = $(e.currentTarget);
		var isDir = target.parent().attr("data-isDir");
		var id = target.parent().attr("data-id");
		if (isDir == "true") {
			console.log(id);
			if (id == "null") getlist();
			else getlist(id);
		}
		else {
			window.open("/GetFile.aspx?file_id=" + id, "_blank");
		}
	});
	var new_folder = $("<li class='MenuItem'><span class='glyphicon glyphicon-folder-open'></span>&nbsp;새폴더</li>");
	new_folder.click(function (e) {
		CancelContextMenu();
		var target = $(e.currentTarget);
		var dir_name = prompt("새로 만들 폴더의 이름 : ");
		if (dir_name.length == 0) {
			alert("폴더의 이름의 길이는 최소한 한글자 이상이어야 합니다.");
		}
		else {
			var data = new FormData();
			data.append("dir_name", dir_name);
			if (v(current_dir_id)) data.append("parent_id", current_dir_id);

			$.ajax({
				url: "/ajax.asmx/CreateDirectory",
				method: 'POST',
				data: data,
				processData: false,
				contentType: false
			}).done(function (data) {
				if (data.code == 1) {
					getlist(current_dir_id);
				}
				else {
					alert(data.message + "\n" + data.reason);
				}
			}).fail(function () {
				alert("실패");
			});
		}
	});
	var delete_btn = $("<li class='MenuItem'><span class='glyphicon glyphicon-remove-circle'></span>&nbsp;삭제</li>");
	delete_btn.click(function (e) {
		CancelContextMenu();
		var target = $(e.currentTarget);
		var id = target.parent().attr("data-id");
		var isDir = target.parent().attr("data-isDir");
		var del_confirm = confirm("정말로 삭제하시겠습니까?");
		if (del_confirm) {
			var data = new FormData();
			data.append("target_id", id);
			data.append("isDir", isDir);
			$.ajax({
				url: "/ajax.asmx/DeleteItem",
				method: 'POST',
				data: data,
				processData: false,
				contentType: false
			}).done(function (data) {
				if (data.code == 1) {
					alert("삭제되었습니다.");
					getlist(current_dir_id);
				}
				else {
					alert(data.message + "\n" + data.reason);
				}
			}).fail(function () {
				alert("실패");
			});
		}
	});
	new_menu.append(file);
	new_menu.append(new_folder);
	new_menu.append(delete_btn);
	$("#CloudContext").append(new_menu);
	new_menu.hide();
	new_menu.offset({ top: y, left: x });

	new_menu.slideDown();

	current_context_menu = new_menu;
}

function CancelContextMenu() {
	var ret = v(current_context_menu);
	if (ret) {
		current_context_menu.remove();
		current_context_menu = undefined;
	}
	return ret;
}

function download_file(file_id) {

}
