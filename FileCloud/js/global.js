﻿$("html").ready(function () {
	var list_item = $("ul.nav.navbar-nav.static > li").toArray();
	list_item.forEach(function (val) {
		if ($(val).find("a").hasClass("selected"))
			$(val).addClass("active");
	});	
	$("#navbar + div").css("clear", "none");
});