﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileCloud
{
	public enum JsonObjectType {
		@string,
		number,
		array,
		@object,
		boolean,
		@null
	}

	public class JsonObject {
		public JsonObjectType Type;
		public string Name;
		public object Value;
		public JsonObject Parent;

		public bool isNull { get { return this.Type == JsonObjectType.@null; } set { } }

		public JsonObject this[int index] {
			get {
				if(this.Type == JsonObjectType.array) {
					return ((List<JsonObject>)this.Value)[index];
				}
				else if(this.Type == JsonObjectType.@object) {
					return ((Dictionary<string, JsonObject>)this.Value)[index.ToString()];
				}
				else {
					throw new IndexOutOfRangeException();
				}
			}
			set {
				if(this.Type == JsonObjectType.array) {
					List<JsonObject> tmp = (List<JsonObject>)this.Value;
					tmp[index] = value;
					this.Value = (object)tmp;
				}
				else if(this.Type == JsonObjectType.@object) {
					Dictionary<string, JsonObject> tmp = (Dictionary<string, JsonObject>)this.Value;
					tmp[index.ToString()] = value;
					this.Value = (object)tmp;
				}
				else {
					throw new IndexOutOfRangeException();
				}
			}
		}

		public JsonObject this[string index] {
			get {
				if(this.Type == JsonObjectType.array) {
					try {
						return ((List<JsonObject>)this.Value)[int.Parse(index)];
					}
					catch(FormatException e) {
						throw new IndexOutOfRangeException(e.Message, e);
					}
				}
				else if(this.Type == JsonObjectType.@object) {
					return ((Dictionary<string, JsonObject>)this.Value)[index];
				}
				else {
					throw new IndexOutOfRangeException();
				}
			}
			set {
				if(this.Type == JsonObjectType.array) {
					try {
						List<JsonObject> tmp = (List<JsonObject>)this.Value;
						tmp[int.Parse(index)] = value;
						this.Value = (object)tmp;
					}
					catch(FormatException e) {
						throw new IndexOutOfRangeException("", e);
					}
				}
				else if(this.Type == JsonObjectType.@object) {
					Dictionary<string, JsonObject> tmp = (Dictionary<string, JsonObject>)this.Value;
					tmp[index] = value;
					this.Value = (object)tmp;
				}
				else {
					throw new IndexOutOfRangeException();
				}
			}
		}

		public JsonObject() {
			this.Type = JsonObjectType.@object;
			InitializeValue();
        }

		public JsonObject(JsonObjectType type) {
			this.Type = type;
			InitializeValue();
		}

		public JsonObject(object value, JsonObjectType type) {
			this.Value = value;
			this.Type = type;
		}

		public JsonObject(string name) {
			this.Name = name;
		}

		public JsonObject(string name, JsonObjectType type) {
			this.Name = name;
			this.Type = type;
			InitializeValue();
		}

		public JsonObject(string name, object value, JsonObjectType type) {
			this.Name = name;
			this.Value = value;
			this.Type = type;
		}

		public void InitializeValue() {
			switch(this.Type) {
				case JsonObjectType.array:
					this.Value = new List<JsonObject>();
					break;
				case JsonObjectType.boolean:
					this.Value = false;
					break;
				case JsonObjectType.number:
					this.Value = 0;
					break;
				case JsonObjectType.@object:
					this.Value = new Dictionary<string, JsonObject>();
					break;
				case JsonObjectType.@string:
					this.Value = "";
					break;
			}
		}

		public void AddNode(JsonObject node) {
			try {
				if(node.Name == null) {
					this.ToList().Add(node);
				}
				else {
					this.ToDictionary().Add(node.Name, node);
				}
			}
			catch {
				if(this.Type == JsonObjectType.@object && node.Name == null)
					throw new Exception("object형식의 json에 노드를 추가할때 자식 노드의 이름은 null일 수 없습니다.");
			}
		}

		public Dictionary<string, JsonObject> ToDictionary() {
			if(this.Type == JsonObjectType.@object) {
				return (Dictionary<string, JsonObject>)this.Value;
			}
			else {
				throw new Exception("Dictionary로 변환 가능한 JsonObject가 아닙니다.");
			}
		}

		public bool ToBoolean() {
			if(this.Type == JsonObjectType.boolean) {
				return (bool)this.Value;
			}
			else {
				throw new Exception("string으로 변환 가능한 JsonObject가 아닙니다.");
			}
		}

		
		public override string ToString() {
			string ret = "";
			switch(this.Type) {
				case JsonObjectType.array:
					ret = this.Name == null ? "" : String.Concat("\"", this.Name, "\"", ":");
					ret += "[";
					List<JsonObject> arr = this.ToList();
                    for(int idx = 0; idx < arr.Count; idx++) {
						ret += arr[idx].ToString();
						if(idx != arr.Count - 1) ret += ",";
					}
					ret += "]";
					break;
				case JsonObjectType.boolean:
					ret = this.Name == null ? "" : String.Concat("\"", this.Name, "\"", ":");
					ret += this.ToBoolean() ? "true" : "false";
					break;
				case JsonObjectType.@null:
					ret = this.Name == null ? "" : String.Concat("\"", this.Name, "\"", ":");
					ret += "null";
					break;
				case JsonObjectType.number:
					ret = this.Name == null ? "" : String.Concat("\"", this.Name, "\"", ":");
					ret += this.Value.ToString();
					break;
				case JsonObjectType.@object:
					ret = this.Name == null ? "" : String.Concat("\"", this.Name, "\"", ":");
					ret += "{";
					Dictionary<string, JsonObject> dic = this.ToDictionary();
					int i = 0;
					foreach(KeyValuePair<string, JsonObject> pair in dic) {
						ret += pair.Value.ToString();
						if(i != dic.Count - 1) ret += ",";
						i++;
					}
					ret += "}";
					break;
				case JsonObjectType.@string:
					string str_val = "";
					byte[] encoded = Encoding.BigEndianUnicode.GetBytes(this.Value.ToString());
					for(int j = 0; j < encoded.Length / 2; j++) {
						if(encoded[j*2] != 0) {
							str_val += String.Format("\\u{0:X2}{1:X2}", encoded[j * 2], encoded[j * 2 + 1]);
						}
						else {
							str_val += Encoding.BigEndianUnicode.GetString(new byte[] { encoded[j * 2], encoded[j * 2 + 1] });
                        }
					}
					
					ret = String.Concat(this.Name == null ? "" : String.Concat("\"", this.Name, "\"", ":"), "\"", str_val, "\"");
					break;
			}

			return ret;
		}

		public double ToDouble() {
			if(this.Type == JsonObjectType.number) {
				return (double)this.Value;
			}
			else {
				throw new Exception("number로 변환 가능한 JsonObject가 아닙니다.");
			}
		}

		public int ToInt() {
			if(this.Type == JsonObjectType.number) {
				return (int)(double)this.Value;
			}
			else {
				throw new Exception("number로 변환 가능한 JsonObject가 아닙니다.");
			}
		}

		public JsonObject[] ToArray() {
			return this.ToList().ToArray();
		}

		public List<JsonObject> ToList() {
			if(this.Type == JsonObjectType.array) {
				return (List<JsonObject>)this.Value;
			}
			else {
				throw new Exception("List로 변환 가능한 JsonObject가 아닙니다.");
			}
		}

		public int[] ToIntArray() {
			JsonObject[] arr = this.ToArray();
			int[] ret = new int[arr.Length];
			for(int i = 0; i < ret.Length; i++) {
				ret[i] = arr[i].ToInt();
			}
			return ret;
		}

		public double[] ToDoubleArray() {
			JsonObject[] arr = this.ToArray();
			double[] ret = new double[arr.Length];
			for(int i = 0; i < ret.Length; i++) {
				ret[i] = arr[i].ToDouble();
			}
			return ret;
		}

		public string[] ToStringArray() {
			JsonObject[] arr = this.ToArray();
			string[] ret = new string[arr.Length];
			for(int i = 0; i < ret.Length; i++) {
				ret[i] = arr[i].ToString();
			}
			return ret;
		}

		public bool KeyExists(int key) {
			return ToList().Count - 1 >= key;
		}

		public bool KeyExists(string key) {
			return ToDictionary().ContainsKey(key);
		}
	}
}
