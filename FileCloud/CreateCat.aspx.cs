﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FileCloud {
	public partial class CreateCat : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {
			Session["user_level"] = Session["user_level"] == null ? "" : Session["user_level"];
			if(!((string)Session["user_level"] == "A"))
				Response.Redirect("/index.aspx");
        }

		protected void Button1_Click(object sender, EventArgs e) {
			SqlManager.Query("INSERT INTO [Category] ([cat_name], [cat_posted]) VALUES ('" + TextBox1.Text + "', 0)");
		}
	}
}