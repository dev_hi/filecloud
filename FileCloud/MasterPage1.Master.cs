﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace FileCloud
{
    public partial class MasterPage1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			Session["login_success"] = Session["login_success"] == null ? false : Session["login_success"];
			Session["user_id"] = Session["user_id"] == null ? "" : Session["user_id"];
			Session["user_level"] = Session["user_level"] == null ? "" : Session["user_level"];

			navbar.Items.Clear();
			XmlDocument menu = new XmlDocument();
			menu.Load(Server.MapPath("/SiteMap.xml"));
			if((bool)Session["login_success"]) {
				string perm = (string)Session["user_level"] == "A" ? "admin" : "logged_in";
				foreach(XmlNode node in menu["permissions"][perm].ChildNodes) {
					MenuItem item = new MenuItem(node.Attributes["name"].Value);
					item.NavigateUrl = node.Attributes["navigateUrl"].Value;
                    navbar.Items.Add(item);
				}
				username_label.Text = Session["username"].ToString();
				Panel1.Visible = false;
				Panel2.Visible = true;
			}
			else {
				foreach(XmlNode node in menu["permissions"]["guest"].ChildNodes) {
					MenuItem item = new MenuItem(node.Attributes["name"].Value);
					item.NavigateUrl = node.Attributes["navigateUrl"].Value;
					navbar.Items.Add(item);
				}
				Panel1.Visible = true;
				Panel2.Visible = false;
			}

			foreach(MenuItem item in navbar.Items) {
				if(Page.ResolveUrl(item.NavigateUrl) == Request.Url.AbsolutePath) item.Selected = true;
			}

			foreach(ListItem item in BulletedList1.Items) {
				item.Value = "/board.aspx?cid=" + item.Value;
			}
        }

		protected void LogoutButton_Click(object sender, EventArgs e) {
			Session.RemoveAll();
			Response.Redirect("/index.aspx");
		}
	}
}